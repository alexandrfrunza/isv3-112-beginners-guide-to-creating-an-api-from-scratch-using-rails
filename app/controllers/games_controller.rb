# frozen_string_literal: true

class GamesController < ApplicationController
  def index
    @games = Game.all
    render json: @games
  end

  def show
    @game = Game.find(params[:id])
    render json: @game
  end

  def create
    @game = Game.create(my_params)
    render json: @game
  end

  def update
    @game = Game.find(params[:id])
    @game.update(my_params)
    render json: @game
  end

  def destroy
    @games = Game.all
    @game = Game.find(params[:id])
    @game.destroy
    render json: @games
  end

  private

  def my_params
    params.permit(:name, :developed_by, :release_year, :platforms)
  end
end
