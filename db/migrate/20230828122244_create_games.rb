# frozen_string_literal: true

class CreateGames < ActiveRecord::Migration[7.0]
  def change
    create_table :games do |t|
      t.string :name
      t.string :developed_by
      t.integer :release_year
      t.string :platforms
      t.timestamps
    end
  end
end
