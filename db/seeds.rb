# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
Game.create(name: 'Grand Theft Auto V', developed_by: 'Rockstar North', release_year: 2013,
            platforms: 'PlayStation 3, Xbox 360, PlayStation 4, Xbox One, Windows, PlayStation 5, Xbox Series X/S')
Game.create(name: 'PUBG: Battlegrounds', developed_by: 'PUBG Studios', release_year: 2017,
            platforms: 'Windows, Android, iOS, Xbox One, PlayStation 4, Stadia, Xbox Series X/S, PlayStation 5')
Game.create(name: 'Overwatch', developed_by: 'Blizzard Entertainment', release_year: 2016,
            platforms: 'PlayStation 4, Windows, Xbox One, Nintendo Switch')
Game.create(name: 'Call of Duty', developed_by: 'Infinity Ward', release_year: 2003,
            platforms: 'Windows, OS X, Nintendo DS, GameCube, Nokia N-Gage, PlayStation 2, PlayStation 3, PlayStation 4, PlayStation 5, PlayStation Portable, PlayStation Vita, Wii, Wii U, Xbox, Xbox 360, Xbox One, Xbox Series X/S, iOS, Android, BlackBerry, J2ME')
Game.create(name: 'Dota 2', developed_by: 'Valve', release_year: 2013, platforms: 'Windows, Linux, OS X')
Game.create(name: 'Minecraft', developed_by: 'Mojang Studios', release_year: 2011,
            platforms: 'Windows, macOS, Linux')
Game.create(name: 'Fortnite', developed_by: 'Epic Games', release_year: 2017,
            platforms: 'macOS[c], Windows, PlayStation 4, Xbox One, iOS[c], Nintendo Switch, Android[c], Xbox Series X/S, PlayStation 5')
