# ISV3-112 beginners-guide-to-creating-an-api-from-scratch-using-rails

API with popular games, information contains names, developers, year of release and what platforms that game can be played.

## Installation

Download full archive from [gitlab](https://gitlab.com/alexandrfrunza/isv3-112-beginners-guide-to-creating-an-api-from-scratch-using-rails/-/archive/ISV3-112_dev_api_games/isv3-112-beginners-guide-to-creating-an-api-from-scratch-using-rails-ISV3-112_dev_api_games.zip) and unzip it to empty folder.

## How to run

Use your terminal and navigate to unziped folder.

```bash
cd path/to/unziped/folder
```

run docker-compose 

```bash
docker-compose up -d
```
create db


run migrations

```bash
docker-compose exec app rails db:migrate
```

run your seeds

```bash
docker-compose exec app rails db:seed
```
## Using api


Find in unziped folder file:

```bash
games.postman_collection.json
```

Import it in postman.
In that imported file you can find get/post/put/delete instance comands. 